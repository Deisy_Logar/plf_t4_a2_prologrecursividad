/*
	Fibonaci
	
	fibo(n-1) + fibo(n-2)
*/


fibonaci(0,0).
fibonaci(1,1).
fibonaci(N,R) :- N>1,
	A is N-1, B is N-2,
	fibonaci(A,T1), fibonaci(B,T2),
	R is T1+T2.
/*
	Ejemplo 1.-
	
	Resultado:
	:-fibonaci(3,R).
	true.
	
	R = 2 :-;
	false.
----------------------------
	
	Ejemplo 2.-
	
	Resultado:
	:-fibonaci(4,R).
	true.
	
	R = 3 :-;
	false.
----------------------------
	
	Ejemplo 3.-
	
	Resultado:
	:-fibonaci(5,R).
	true.
	
	R = 5 :-;
	false.
*/

/*
	Multiplicación Recursiva
*/

multiplicacion(_, B, 0) :- B=0.
/*
	Resultado:
	:-multiplicacion(A, 0, R).
	true.
	
	R = 0.
----------------------------
	
	Resultado:
	:-multiplicacion(_,0,R).
	true.
	
	R = 0.
*/
multiplicacion(A,B,R) :- B>0,
	T is B-1,
	multiplicacion(A,T,P),
	R is A+P.
/*
	Ejemplo 1.-
	
	Resultado:
	:-multiplicacion(3,2,R).
	true.
	
	R = 6 :-;
	false.
----------------------------
	
	Ejemplo 2.-
	
	Resultado:
	:-multiplicacion(2,4,R).
	true.
	
	R = 8 :-;
	false.
*/



/*
	Ejemplo de factorial en Java
	
	public static int factorial(int num){
		if(num == 0){
			return 1;
		} else {
			return num * factorial(num-1);
		}
	}
*/

factorial(0,1).
factorial(N, Salida) :- T is N-1, factorial(T, S1), Salida is N*S1.
/*
	Ejemplo 1.-
	
	Resultado:
	:-factorial(5,R).
	true.
	
	R = 120 :-;
	false.
----------------------------
	
	Ejemplo 2.-
	
	Resultado:
	:-factorial(3,R).
	true.
	
	R = 6 :-;
	false.
*/

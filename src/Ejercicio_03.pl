/*
	Ejemplo Divisi�n Recursivo en Java
	
	int division (int a, int b){
		if(b > a){
			return 0;
		} else {
			return division(a-b, b) + 1;
		}
	}
*/


division(A,B,0) :- B>A.
division(A,B,Sa) :- T is A-B, division(T,B,P), Sa is P+1.
/*
	Ejemplo 1.-
	
	Resultado:
	:-division(8,4,R).
	true.
	
	R = 2 :-;
	false.
----------------------------
	
	Ejemplo 2.-
	
	Resultado:
	:-division(8,2,R).
	true.
	
	R = 4 :-;
	false.
*/

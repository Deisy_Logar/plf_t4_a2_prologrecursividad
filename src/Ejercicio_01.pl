/*
	Algoritmo de potencia en JAVA
	
	int potencia(int base, int exponente){
		if(exponente == 0){
			return 1
		} else {
			return base * potencia(base, exponente - 1)
		}
	}
*/

potencia(A, 0, 1) :- not(A=0).
potencia(X,Y,P) :- Y>0, Y1 is Y-1, potencia(X,Y1,P1), P is X*P1.

/*
	Ejemplo 1.-
	
	Resultado:
	:-potencia(2,3,R).
	true.
	
	R = 8 :-;
	false.
----------------------------
	
	Ejemplo 2.-
	
	Resultado:
	:-potencia(4,3,R).
	true.
	
	R = 64 :-;
	false.
----------------------------
	
	Ejemplo 3.-
	
	Resultado:
	:-potencia(4,2,R).
	true.
	
	R = 16 :-;
	false.
*/

